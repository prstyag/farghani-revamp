<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('location') &mdash; {{ config('app.name') }}</title>

    @include('layouts.admin.components.header')

</head>

<body>
    <div id="app">
        <div class="main-wrapper">
            <div class="navbar-bg"></div>

            <!-- Main navbar -->
            @include('layouts.admin.components.navbar')

            <!-- Main navbar -->
            @include('layouts.admin.components.sidebar_menu')

            <!-- Main Content -->
            @include('layouts.admin.components.content')
        
            <!-- Main navbar -->
            @include('layouts.admin.components.footer')

        </div>
    </div>

    @include('layouts.admin.components.plugin')

    <!-- Sweet Alert Plagin --->
    @include('sweetalert::alert')
</body>
</html>
