@extends('layouts.admin.app')
@section('sub-title', 'Permission')
@section('location', 'Permission')
@push('css')
@endpush
@section('content') 

    <div class="card">
        <div class="card-header">
            <h4><a href="{{route('admin.permission.create')}}" class="btn btn-info">Create Permission</a></h4>
            <div class="card-header-action">
              <form action="{{route('admin.permission.index')}}" method="get">
                <div class="input-group">
                  <input type="text" name="filter[name]" class="form-control" placeholder="Search by name" value="">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>
                  </div>
                </div>
              </form>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
              <table class="table table-striped" id="table-2">
                <thead>
                  <tr>
                    <th class="text-center">
                      <div class="custom-checkbox custom-control">
                        <input type="checkbox" data-checkboxes="mygroup" data-checkbox-role="dad" class="custom-control-input" id="checkbox-all" onclick="toggle(this);">
                        <label for="checkbox-all" class="custom-control-label">&nbsp;</label>
                      </div>
                    </th>
                    <th>Name</th>
                    <th>Display Name</th>
                    <th>Guard Name</th>
                    <th>Created on</th>
                    <th>Action</th>
                  </tr>
                </thead>
                @if (count($permission) > 0)
                @foreach ($permission as $p)
                <tbody>
                  <tr>
                    <td>
                      <div class="custom-checkbox custom-control">
                        <input type="checkbox" data-checkboxes="mygroup" class="custom-control-input" id="checkbox-1">
                        <label for="checkbox-1" class="custom-control-label">&nbsp;</label>
                      </div>
                    </td>
                    <td>{{$p->name}}</td>
                    <td>{{$p->display_name}}</td>
                    <td>{{$p->guard_name}}</td>
                    <td>{{$p->created_at}}</td>
                    <td>
                      <a href="#" class="btn btn-sm btn-info trigger--fire-modal-6" data-toggle="modal" data-target="#permission{{$p->id}}">Detail</a>
                      <a href="{{route('admin.permission.edit', $p->id)}}" class="btn btn-sm btn-primary">Edit</a>
                    </td>
                  </tr>
                </tbody>
                @endforeach
                @else
                <td colspan="9" class="text-center"><h4>Permission is not Found!</h4></td>
                @endif
            </table>
          </div>
        </div>
    </div>

    <!-- Pagination -->
    <nav aria-label="Page navigation example">
      <ul class="pagination">
        <li class="page-item">
          {{$permission->links('vendor.pagination.bootstrap-4')}}
        </li>
      </ul>
    </nav>
@endsection
@section('modal')
<!-- Modal -->
@foreach ($permission as $p)
<div class="modal fade" id="permission{{$p->id}}" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel" style="color: #6777ef">Details:</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <label for="name" class="col-sm-2 col-form-label">Name</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" value="{{$p->name}}" disabled>
          </div>
        </div>
        <div class="form-group row">
          <label for="username" class="col-sm-2 col-form-label">Display Name</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" value="{{$p->display_name}}" disabled>
          </div>
        </div>
        <div class="form-group row">
          <label for="inputEmail3" class="col-sm-2 col-form-label">Guard Name</label>
          <div class="col-sm-10">
            <input type="email" class="form-control" value="{{$p->guard_name}}" disabled>
          </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="status">Create on</label>
                <input type="text" name="status" class="form-control" 
                value="{{$p->created_at}}"
                disabled>
            </div>
            <div class="form-group col-md-6">
                <label for="status">Update on</label>
                <input type="text" name="status" class="form-control" 
                value="{{$p->updated_at}}"
                disabled>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
        <a href="{{route('admin.permission.edit', $p->id)}}" class="btn btn-primary">Edit</a>
      </div>
    </div>
  </div>
</div>
@endforeach
@endsection
@push('scripts')
<script>
// Selected All Cheackbox
function toggle(source) {
    var checkboxes = document.querySelectorAll('input[type="checkbox"]');
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i] != source)
            checkboxes[i].checked = source.checked;
    }
};
</script>
@endpush
