@extends('layouts.admin.app')
@section('sub-title', 'Profile')

@section('location')  
@if (get_action_name() == 'create')
Add Permission
@else
Edit Permission
@endif
@endsection

@push('links')
<style>
    .isDisabled {
        cursor: not-allowed;
        opacity: 0.5;
        color: red;
    }
</style>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css"> 
@endpush
@section('content') 
<div class="card">
    <form action="{{
    route((get_action_name() == 'create')? 'admin.permission.store': 'admin.permission.update', [
        'permission'   => $permission ?? '',
        ])
    }}" 
    method="POST">
    @csrf
    @method((get_action_name() == 'create') ? 'POST': 'PUT')

    <div class="card-header">
        <h4><a href="{{route('admin.permission.index')}}" class="btn btn-warning">Back</a></h4>
        @if (!(get_action_name() == 'create'))
        <div class="card-header-action">
            <div class="dropdown">
              <a href="#" class="dropdown-toggle btn btn-primary" data-toggle="dropdown" aria-expanded="false">Advanced</a>
              <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(70px, 25px, 0px);">
                @role('super_admin|admin')
                <a href="#" class="dropdown-item has-icon" data-toggle="modal" data-target="#delete{{$permission->id}}"><i class="fas fa-trash"></i>Delete Permission</a>
                @else
                <a href="javascript:void(0)" class="dropdown-item has-icon isDisabled"><i class="fas fa-user-times"></i>You don't have access</a>
                @endrole
              </div>
            </div>
        </div>
        @endif
    </div>
    <div class="card-body">
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" name="name" class="form-control" placeholder="Create this name.." value="{{ $permission->name ?? ''}}">
        </div>
        <div class="form-group">
            <label for="display_name">Display Name</label>
            <input type="text" name="display_name" class="form-control" placeholder="Create this display name.." value="{{ $permission->display_name ?? ''}}">
        </div>
        <div class="form-group">
            <label for="guard_name">Guard Name</label>
            <input type="text" name="guard_name" class="form-control" placeholder="Create this guard name.." value="{{ $permission->guard_name ?? ''}}">
        </div>
    </div>
    <div class="card-footer">
      <button type="submit" class="btn btn-primary">
          {{(get_action_name() == 'create') ? 'Submit': 'Update'}}
      </button>
    </div>
    </form>
</div>
@endsection
@section('modal')
<!-- Modal -->
<div class="modal fade" id="delete{{$permission->id ?? ''}}" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLabel" style="color: #6777ef">Confirmation:</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <h4 class="modal-title">Are you sure to remove this permission?</h4>
            <h4 class="modal-title text-center" style="color: red; text-decoration: underline">{{$permission->display_name ?? ''}}</h4>
        </div>
        <div class="modal-footer">
            <form action="{{ route('admin.permission.destroy', $permission->id ?? '') }}" method="POST">
                @csrf
                @method('delete')
                <button type="button" class="btn btn-warning" data-dismiss="modal">No, and Close</button>
                <button type="submit" class="btn btn-danger">Sure, and Delete</button>
            </form>
        </div>
    </div>
  </div>
</div>
@endsection
@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
@endpush
