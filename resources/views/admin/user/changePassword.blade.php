@extends('layouts.admin.app')
@section('sub-title', 'Management Password')
@section('location', 'Management Password')
@section('content') 
<div class="card">
    <form action="{{route('admin.change-password', $user->id)}}" method="POST">
    @csrf
    @method('PUT')
    <div class="card-header">
        <h4>Change Password</h4>
    </div>
    <div class="card-body">
        <div class="form-group">
          <label for="email">Email</label>
          <input type="email" name="email" class="form-control" placeholder="{{$user->email}}" disabled>
        </div>
        <div class="form-group">
            <label for="old_password">Old Password</label>
            <div class="input-group">
                <input type="password" name="old_password" class="form-control">
                <div class="input-group-text toggle-password">
                    <i class="fa fa-fw fa-eye-slash"></i>
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="password">New Password</label>
                <div class="input-group">
                    <input type="password" name="password" id="password" class="form-control" placeholder="Create New Password ...">
                    <div class="input-group-text toggle-password">
                        <i class="fa fa-fw fa-eye-slash"></i>
                    </div>
                </div>
            </div>
            <div class="form-group col-md-6">
                <label for="confirm_password">Password Confirm</label>
                <div class="input-group">
                    <input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="Confirm New Password ...">
                    <div class="input-group-text toggle-password">
                        <i class="fa fa-fw fa-eye-slash"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
      <a href="{{route('admin.my-profile', $me->id)}}" class="btn btn-warning">Back</a>
    </div>
    </form>
</div>
@endsection
@push('scripts')
<script>
// View Password
$('.toggle-password').click(function(){
    $(this).children().toggleClass('fa-eye-slash fa-eye');
    let input = $(this).prev();
    input.attr('type', input.attr('type') === 'password' ? 'text' : 'password');
});    
</script>    
@endpush
