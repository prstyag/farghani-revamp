@extends('layouts.admin.app')
@section('sub-title', 'User')
@section('location', 'User')
@push('links')
<!-- MDBootstrap Datatables  -->
<link href="{{asset('assets/css/datatables.min.css')}}" rel="stylesheet">
@endpush
@section('content') 
    <div class="card">
        <div class="card-header">
            <h4><a href="{{route('admin.users.create')}}" class="btn btn-info">Create User</a></h4>
            <div class="card-header-action" style="display: flex">
              <form action="{{route('admin.users.index')}}" method="get">
                <div class="input-group">
                  <input type="text" name="filter[name]" class="form-control" placeholder="Search by name" value="">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>
                  </div>
                </div>
              </form>
              <div class="card-header-action dropdown" style="margin-left: 2em">
                <a href="#" data-toggle="dropdown" class="btn btn-danger dropdown-toggle" aria-expanded="false">Select</a>
                <ul class="dropdown-menu dropdown-menu-sm dropdown-menu-right" x-placement="bottom-end" style="position: absolute; transform: translate3d(75px, 31px, 0px); top: 0px; left: 0px; will-change: transform;">
                  <li class="dropdown-title">Select by</li>
                  <li><a href="#" class="dropdown-item">Username</a></li>
                  <li><a href="#" class="dropdown-item">Name</a></li>
                  <li><a href="#" class="dropdown-item">Roles</a></li>
                  <li><a href="#" class="dropdown-item">Join On</a></li>
                </ul>
              </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
              <table class="table table-striped" id="table-user">
                <thead>
                  <tr>
                    <th class="text-center">
                      <div class="custom-checkbox custom-control">
                        <input type="checkbox" data-checkboxes="mygroup" data-checkbox-role="dad" class="custom-control-input" id="checkbox-all" onclick="toggle(this);">
                        <label for="checkbox-all" class="custom-control-label">&nbsp;</label>
                      </div>
                    </th>
                    <th>Username</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Profile</th>
                    <th>Join On</th>
                    <th>Roles</th>
                    <th>Action</th>
                  </tr>
                </thead>
                @if (count($user) > 0)
                @foreach ($user as $data)
                <tbody>
                  <tr>
                    <td>
                      <div class="custom-checkbox custom-control">
                        <input type="checkbox" data-checkboxes="mygroup" class="custom-control-input" id="checkbox-1">
                        <label for="checkbox-1" class="custom-control-label">&nbsp;</label>
                      </div>
                    </td>
                    <td>{{$data->username}}</td>
                    <td>{{$data->name}}</td>
                    <td>{{$data->email}}</td>
                    <td>
                      <img alt="image" src="../assets/img/avatar/avatar-5.png" class="rounded-circle" width="35" data-toggle="tooltip" title="" data-original-title="Wildan Ahdian">
                    </td>
                    <td>{{$data->created_at->format('d/m/Y')}}</td>
                    <td>
                      @forelse ($data->roles as $item)
                        <div class="badge badge-light">{{$item->display_name}}</div>
                      @empty
                        <div class="badge badge-danger">No Roles</div>
                      @endforelse
                    </td>
                    <td>
                      <a href="#" class="btn btn-sm btn-info trigger--fire-modal-6" data-toggle="modal" data-target="#users{{$data->id}}">Detail</a>
                      <a href="{{route('admin.users.edit', $data->id)}}" class="btn btn-sm btn-primary">Edit</a>
                    </td>
                  </tr>
                </tbody>
                @endforeach
                @else
                <td colspan="9" class="text-center"><h4>User is not Found!</h4></td>
                @endif
            </table>
          </div>
        </div>
    </div>

    <!-- Pagination -->
    <nav aria-label="Page navigation example">
      <ul class="pagination">
        <li class="page-item">
          {{$user->links('vendor.pagination.bootstrap-4')}}
        </li>
      </ul>
    </nav>
@endsection
@section('modal')
<!-- Modal -->
@foreach ($user as $data)
<div class="modal fade" id="users{{$data->id}}" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel" style="color: #6777ef">Details:</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <label for="name" class="col-sm-2 col-form-label">Name</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" value="{{$data->name}}" disabled>
          </div>
        </div>
        <div class="form-group row">
          <label for="username" class="col-sm-2 col-form-label">Username</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" value="{{$data->username}}" disabled>
          </div>
        </div>
        <div class="form-group row">
          <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
          <div class="col-sm-10">
            <input type="email" class="form-control" value="{{$data->email}}" disabled>
          </div>
        </div>
        <div class="form-group row">
          <label for="address" class="col-sm-2 col-form-label">Address</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" value="{{$data->address ?? 'Address not found'}}" disabled>
          </div>
        </div>
        <div class="form-group row">
          <label for="phone" class="col-sm-2 col-form-label">Phone</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" value="{{$data->phone ?? 'Phone number not found'}}" disabled>
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="status">Status</label>
            <input type="text" name="status" class="form-control" 
            value="{{($data->status ?? '') == 1 ? 'Active':'Inactive'}}"
            disabled>
          </div>
          <div class="form-group col-md-6">
            <label for="roles">Roles</label>
            @forelse ($data->roles as $item)
              <input type="text" name="roles" class="form-control" value="{{$item->display_name}}" disabled>
            @empty
              <input type="text" name="roles" class="form-control" value="No Roles" disabled>
            @endforelse
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
        <a href="{{route('admin.users.edit', $data->id)}}" class="btn btn-primary">Edit</a>
      </div>
    </div>
  </div>
</div>
@endforeach
@endsection
@push('scripts')
<!-- MDBootstrap Datatables  -->
<script type="text/javascript" src="{{asset('assets/js/pages/datatables.min.js')}}"></script>
<script>
  // Selected All Cheackbox
$(document).ready(function () {
$('#table-user').DataTable();
$('.dataTables_length').addClass('bs-select');
});
  // Selected All Cheackbox
function toggle(source) {
    var checkboxes = document.querySelectorAll('input[type="checkbox"]');
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i] != source)
            checkboxes[i].checked = source.checked;
    }
};
</script>
@endpush