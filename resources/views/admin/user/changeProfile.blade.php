@extends('layouts.admin.app')
@section('sub-title', 'Profile')
@section('location', 'Edit Profile')  
@push('links')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css"> 
@endpush
@section('content') 
<div class="card">
    <form action="{{route('admin.my-profile.update', $user->id)}}"
    method="POST">
    @csrf
    @method('PUT')
    <div class="card-header">
        <h4><a href="{{url('admin/users/'.$user->id.'/my-profiles')}}" class="btn btn-warning">Back</a></h4>
        @if (!(get_action_name() == 'create'))
        <div class="card-header-action">
            <div class="dropdown">
              <a href="#" class="dropdown-toggle btn btn-primary" data-toggle="dropdown" aria-expanded="false">Advanced</a>
              <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(70px, 25px, 0px);">
                <a href="{{route('admin.password', $user->id)}}" class="dropdown-item has-icon"><i class="fas fa-key"></i>Change Password</a>
              </div>
            </div>
        </div>
        @endif
    </div>
    <div class="card-body">
        <div class="form-row">
            <div class="form-group col-md-6">
              <label for="name">Name</label>
              <input type="text" name="name" class="form-control" placeholder="Create name.." value="{{ $user->name ?? '' }}">
            </div>
            <div class="form-group col-md-6">
              <label for="name">Username</label>
              <input type="text" name="username" class="form-control" placeholder="Create username.." value="{{ $user->username ?? '' }}">
            </div>
        </div>
        <div class="form-row">
            <div class="{{(get_action_name() == 'create')? 'form-group col-md-6': 'form-group col-md-12'}}">
            <label for="email">Email</label>
            <input type="email" class="form-control" name="email" placeholder="Email" value="{{ $user->email ?? '' }}">
            </div>
        </div>
        <div class="form-group">
            <label for="address">Address</label>
            <input type="text" name="address" class="form-control" placeholder="Apartment, studio, or floor" value="{{$user->address ?? ''}}">
        </div>
        <div class="form-group">
            <label for="phone">Phone Number</label>
            <input type="text" name="phone" class="form-control" placeholder="0896-XXXX-XXXX" value="{{$user->phone ?? ''}}">
        </div>
    </div>
    <div class="card-footer">
      <button type="submit" class="btn btn-primary">
          {{(get_action_name() == 'create') ? 'Submit': 'Update'}}
      </button>
    </div>
    </form>
</div>
@endsection
@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
<script>
// View Password
$('.toggle-password').click(function(){
    $(this).children().toggleClass('fa-eye-slash fa-eye');
    let input = $(this).prev();
    input.attr('type', input.attr('type') === 'password' ? 'text' : 'password');
});
</script>
@endpush
