@extends('layouts.admin.app')
@section('sub-title', 'Profile')

@section('location')  
@if (get_action_name() == 'create')
Add User
@else
Edit User
@endif
@endsection

@push('links')
<style>
    .isDisabled {
        cursor: not-allowed;
        opacity: 0.5;
        color: red;
    }
</style>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css"> 
@endpush
@section('content') 
<div class="card">
    <form action="{{
    route((get_action_name() == 'create')? 'admin.users.store': 'admin.users.update', [
        'user'   => $user ?? '',
        ])
    }}" 
    method="POST">
    @csrf
    @method((get_action_name() == 'create') ? 'POST': 'PUT')

    <div class="card-header">
        <h4><a href="{{route('admin.users.index')}}" class="btn btn-warning">Back</a></h4>
        @if (!(get_action_name() == 'create'))
        <div class="card-header-action">
            <div class="dropdown">
              <a href="#" class="dropdown-toggle btn btn-primary" data-toggle="dropdown" aria-expanded="false">Advanced</a>
              <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(70px, 25px, 0px);">
                @if ( $user->id != $me->id )
                <a href="javascript:void(0)" class="dropdown-item has-icon isDisabled"><i class="fas fa-user-times"></i>You don't have access</a>
                @else
                <a href="{{route('admin.password', $user->id)}}" class="dropdown-item has-icon"><i class="fas fa-key"></i>Change Password</a>
                @endif
              </div>
            </div>
        </div>
        @endif
    </div>
    <div class="card-body">
        <div class="form-row">
            <div class="form-group col-md-6">
              <label for="name">Name</label>
              <input type="text" name="name" class="form-control" placeholder="Create name.." value="{{ $user->name ?? '' }}" required>
            </div>
            <div class="form-group col-md-6">
              <label for="name">Username</label>
              <input type="text" name="username" class="form-control" placeholder="Create username.." value="{{ $user->username ?? '' }}" required>
            </div>
        </div>
        <div class="form-row">
            <div class="{{(get_action_name() == 'create')? 'form-group col-md-6': 'form-group col-md-12'}}">
            <label for="email">Email</label>
            <input type="email" class="form-control" name="email" placeholder="Email" value="{{ $user->email ?? '' }}">
            </div>
            @if ((get_action_name() == 'create'))
            <div class="form-group col-md-6">
                <label for="password">Password</label>
                <div class="input-group">
                    <input type="password" name="password" id="password" class="form-control" placeholder="Password" value="{{ $user->password ?? '' }}" required>
                    <div class="input-group-text toggle-password">
                        <i class="fa fa-fw fa-eye-slash"></i>
                    </div>
                </div>
            </div>
            @endif
        </div>
        <div class="form-group">
            <label for="address">Address</label>
            <input type="text" name="address" class="form-control" placeholder="Apartment, studio, or floor" value="{{$user->address ?? ''}}">
        </div>
        <div class="form-group">
            <label for="phone">Phone Number</label>
            <input type="text" name="phone" class="form-control" placeholder="0896-XXXX-XXXX" value="{{$user->phone ?? ''}}">
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="status">Status</label>
                <select class="selectpicker form-control" name="status" required>
                    <option name="status" value="1" {{ ($user->status ?? '') == '1' ? 'selected': '' }}>Active</option>
                    @if ((get_action_name() !== 'create'))
                    @role('super_admin|admin')
                    <option value="0" {{ ($user->status ?? '') == '0' ? 'selected': '' }}>Inactive</option>
                    @else
                    {{-- if not super_admin|admin not a option... --}}
                    @endrole
                    @endif
                </select>
            </div>
            <div class="form-group col-md-6">
                <label for="inputAddress2">Roles</label>
                <select class="selectpicker form-control" data-live-search="true" name="roles[]">
                    <option value="" disabled selected>Select your role</option>
                    @if (get_action_name() == 'create')
                        @foreach ($roles as $role)
                        <option value="{{$role->name}}">{{$role->display_name}}</option>
                        @endforeach
                    @else
                        @foreach ($roles as $role)
                        <option value="{{$role->name}}" 
                            @foreach ($getUserByRoles as $item)
                                @if ($role->name == $item)
                                    selected
                                @endif
                            @endforeach    
                        >{{$role->display_name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div>
    <div class="card-footer">
      <button type="submit" class="btn btn-primary">
          {{(get_action_name() == 'create') ? 'Submit': 'Update'}}
      </button>
    </div>
    </form>
</div>
@endsection
@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

<script>
// View Password
$('.toggle-password').click(function(){
    $(this).children().toggleClass('fa-eye-slash fa-eye');
    let input = $(this).prev();
    input.attr('type', input.attr('type') === 'password' ? 'text' : 'password');
});
</script>
@endpush