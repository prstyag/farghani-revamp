<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RoleRequest;
use App\Repositories\PermissionRepository;
use App\Repositories\RoleRepository;

class RoleController extends Controller
{

    protected $roleRepository;
    protected $permissionRepository;

    public function __construct(RoleRepository $roleRepository,
                                PermissionRepository $permissionRepository)
    {
        $this->repo = $roleRepository;
        $this->permiss = $permissionRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = $this->repo->getAll();
        return view('admin.roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permission = $this->permiss->getAll();
        return view('admin.roles.form', compact('permission'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        $this->repo->store();
        return redirect()->to('admin/role')->withSuccess('You have successfully added a new roles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission         = $this->repo->getAllPermission();
        $permissionByRoles  = $this->repo->permissionByRoles($id);
        $role               = $this->repo->getById($id);

        return view('admin.roles.form', compact('permission','permissionByRoles', 'role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleRequest $request, $id)
    {
        $this->repo->updateById($id);
        return redirect()->back()->withSuccess('You have successfully updated a roles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repo->delete($id);
        return redirect()->to('admin/role')->withSuccess('You have successfully deleted a roles');
    }
}
