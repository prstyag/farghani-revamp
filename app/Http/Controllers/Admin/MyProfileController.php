<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\UserRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\MyProfileRequest;

class MyProfileController extends Controller
{

    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->user = $userRepository;
    }

    /**
     * get data profile user.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = $this->user->getAll();
        return view('admin.user.profile', compact('user'));
    }

    /**
     * get data profile user.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile($id)
    {
        $user = $this->user->getById($id);
        return view('admin.user.changeProfile', compact('user'));
    }

    /**
     * update data profile user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(MyProfileRequest $request, $id)
    {
        $this->user->updateProfile($id);
        return redirect()->back()->withSuccess('You have successfully updated profile');
    }

    /**
     * get data Only password user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function Password($id)
    {
        $user = $this->user->getById($id);
        return view('admin.user.changePassword', compact('user'));
    }

    /**
     * update Only password user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function changePassword(ChangePasswordRequest $request, $id)
    {
        $this->user->updatePasswordById($id);
        return redirect()->to('admin/users/'.$id.'/my-profiles')->withSuccess('You have successfully updated password');
    }

}
