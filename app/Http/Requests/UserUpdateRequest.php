<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'string|required|min:2',
            'username'      => 'string|required|min:2|max:100|unique:users,username,' . $this->route('user'),
            'email'         => 'string|required|unique:users,email,' . $this->route('user'),
            'address'       => ['nullable', 'string'],
            'phone'         => ['nullable', 'regex:/^((62)|(08))[0-9]/', 'not_regex:/[a-z]/', 'min:11', 'max:13'],
            'status'        => 'required',
            'roles'         => 'required'
        ];
    }
}
