<?php

use Illuminate\Support\Facades\Request;

if(!function_exists('get_action_name')){
    function get_action_name()
    {
        $method = Request::route()->getActionName();
        $name   = explode('@', $method);
        return array_pop($name);
    }
}