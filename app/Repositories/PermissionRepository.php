<?php 

namespace App\Repositories;

use Spatie\Permission\Models\Permission;
use Spatie\QueryBuilder\QueryBuilder;

class PermissionRepository extends BaseRepository
{
    /**
     * Get all user
     *
     * @return Collection
     */
    public function getAll()
    {
        return QueryBuilder::for(Permission::class)
                            ->allowedFilters(['name'])
                            ->orderBy('created_at', 'desc')
                            ->paginate(10)
                            ->appends(request()->query());
    }

    /**
     * Get user by id
     *
     * @param string $permission_id
     * @return Permission
     */
    public function getById(string $permission_id)
    {
        return Permission::where('id', $permission_id)->firstOrFail();
    }

    /**
     * Update user by id
     *
     * @param string $permission_id
     * @return void
     */
    public function updateById(string $permission_id)
    {
        $permission = $this->getById($permission_id);
        $permission->update(request()->all());
    }


    /**
     * Store data permission
     *
     * @return Permission
     */
    public function store()
    {
        $permission = request()->all();
        return Permission::create($permission)->firstOrFail();
    }


    /**
     * Force delete data permission
     *
     * @return Permission
     */
    public function delete(string $permission_id)
    {
        $permission = $this->getById($permission_id);
        $permission->delete();
    }
}