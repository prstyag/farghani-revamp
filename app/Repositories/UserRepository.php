<?php 

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Spatie\QueryBuilder\QueryBuilder;

class UserRepository extends BaseRepository
{
    /**
     * Get all user
     *
     * @return QueryBuilder
     */
    public function getAll()
    {
        return QueryBuilder::for(User::class)
                            ->allowedFilters(['name'])
                            ->orderBy('created_at', 'desc')
                            ->paginate(10)
                            ->appends(request()->query());
    }

    /**
     * Get user by id
     *
     * @return QueryBuilder
     */
    public function getById(string $user_id)
    {
        return User::where('id', $user_id)->firstOrFail();
    }

    /**
     * Store data user
     *
     * @return User
     */
    public function store()
    {
        $data = request()->all();
        $data['password'] = Hash::make(request('password'));

        $user = User::create($data);
        $user->assignRole(request()->input('roles'));

        return $user;
    }

    /**
     * Update data user by id
     *
     * @return User
     */
    public function updateById(string $user_id)
    {
        // $data = $this->getById($user_id);
        $user = User::find($user_id);

        $user->name         = request()->input('name');
        $user->username     = request()->input('username');
        $user->address      = request()->input('address');
        $user->phone        = request()->input('phone');
        $user->avatar       = request()->input('avatar');
        $user->status       = request()->input('status');

        $user->syncRoles(request()->input('roles'));
        $user->save();
    }

    /**
     * Update data profile
     *
     * @return User
     */
    public function updateProfile(string $user_id)
    {
        $user = $this->getById($user_id);
        $user->update(request()->only(['name','username', 'email', 'address', 'phone', 'avatar']));
    }

    /**
     * Update data user only password
     *
     * @return User
     */
    public function updatePasswordById(string $user_id)
    {
        $data = request()->only(['password']);
        $data['password'] = Hash::make($data['password']);
        $user = $this->getById($user_id);
        $user->update($data);
    }

        
    /**
     * get user by roles
     *
     * @param string $user_id
     * @return User
     */
    public function getUserByRoles(string $user_id)
    {
        $user = User::findOrFail($user_id);
        $groupsWithRoles = collect($user->getRoleNames());

        return $groupsWithRoles;
    }
}