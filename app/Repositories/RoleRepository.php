<?php 

namespace App\Repositories;

use Spatie\Permission\Models\Role;
use Spatie\QueryBuilder\QueryBuilder;
use App\Repositories\PermissionRepository;
use Illuminate\Support\Collection;
use Spatie\Permission\Models\Permission;

class RoleRepository extends BaseRepository
{
    protected $permissionRepository;

    public function __construct(PermissionRepository $permissionRepository)
    {
        $this->permis = $permissionRepository;
    }

    /**
     * Get all role
     *
     * @return QueryBuilder
     */
    public function getAll()
    {
        return QueryBuilder::for(Role::class)
                ->allowedFilters(['name'])
                ->orderBy('created_at', 'desc')
                ->paginate(10)
                ->appends(request()->query());
    }

    /**
     * Get role by id
     *
     * @return QueryBuilder
     */
    public function getById(string $roles_id)
    {
        return Role::where('id', $roles_id)->firstOrFail();
    }

    /**
     * update role by id
     *
     * @return QueryBuilder
     */
    public function updateById(string $roles_id)
    {
        $role = $this->getById($roles_id);
        $role->update(request()->all());
        $role->syncPermissions(request()->input('permissions'));
    }

    /**
     * store role
     *
     * @return QueryBuilder
     */
    public function store()
    {
        $role = Role::create([
            'name' => request()->input('name'),
            'display_name' => request()->input('display_name'),
            'guard_name' => request()->input('guard_name')
        ]);
        $role->givePermissionTo(request()->input('permissions'));
    }

    /**
     * get permission by roles
     *
     * @param string $permission
     * @return Permission
     */
    public function permissionByRoles(string $roles_id): Collection
    {
        $role = Role::findOrFail($roles_id);
        $groupsWithRoles = collect($role->getPermissionNames());

        return $groupsWithRoles;
    }

    /**
     * get permission all
     *
     * @param string $permission
     * @return Permission
     */
    public function getAllPermission()
    {
        return Permission::all();
    }
    
    /**
     * Force delete roles
     *
     * @param string $roles
     * @return Roles
     */
    public function delete(string $roles_id)
    {
        $permissionByRoles = $this->permissionByRoles($roles_id);

        $role = $this->getById($roles_id);
        $role->revokePermissionTo($permissionByRoles);

        $role->delete();
    }
}