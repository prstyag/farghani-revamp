<?php
namespace App\View\Composers;

use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class MyProfileComposer
{
    /**
     * The user repository implementation.
     *
     * @var \Illuminate\Support\Facades\Auth
     */
    protected $auth;

    /**
     * Create a new profile composer.
     *
     * @param \Illuminate\Support\Facades\Auth $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->users = $auth::user();
    }

    /**
     * Bind data to the view.
     *
     * @param \Illuminate\View\View $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('me', $this->users);
    }
}