<?php

// use App\Http\Controllers\Admin\UserController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return view('welcome');
});

// Route::get('export/excel', [UserController::class, 'exportExcel'])->name('excel');
// Route::get('export/csv', [UserController::class, 'exportCsv'])->name('csv');
// Route::get('export/pdf', [UserController::class, 'exportPdf'])->name('pdf');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

require __DIR__.'/admin.php';