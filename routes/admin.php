<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\MyProfileController;
use App\Http\Controllers\Admin\PermissionController;
use App\Http\Controllers\Admin\RoleController;



Route::prefix('admin')->name('admin.')->group(function () {
    Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::resource('users', UserController::class);

    Route::get('users/{id}/my-profiles',         [MyProfileController::class, 'index'])->name('my-profile');
    Route::get('users/{id}/my-profiles-edit',    [MyProfileController::class, 'profile'])->name('my-profile.edit');
    Route::put('users/{id}/my-profiles-update',  [MyProfileController::class, 'updateProfile'])->name('my-profile.update');
    Route::get('users/{id}/password',            [MyProfileController::class, 'Password'])->name('password');
    Route::put('users/{id}/change-password',     [MyProfileController::class, 'changePassword'])->name('change-password');

    Route::resource('permission', PermissionController::class);
    Route::resource('role', RoleController::class);
});
