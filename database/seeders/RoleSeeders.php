<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleSeeders extends Seeder
{
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Created role Super Admin
        $superAdmin = Role::create(
            [
                'name'                  => 'super_admin',
                'display_name'          => 'Super Admin',
                'guard_name'            => 'web'
            ]
        );
        $superAdmin->givePermissionTo(Permission::all());

        // Created role Admin
        $admin      = Role::create(
            [
                'name'                  => 'admin',
                'display_name'          => 'Admin',
                'guard_name'            => 'web'
            ]
        );
        $admin->givePermissionTo([
            'users_create',
            'users_edit',
            'users_show',
        ]);

        // Created role Writer
        $writer     = Role::create(
            [
                'name'                  => 'writer',
                'display_name'          => 'Writer',
                'guard_name'            => 'web'
            ]
        );
        $writer->givePermissionTo([
            'users_edit',
            'users_create',
        ]);

        // Created role User
        $user     = Role::create(
            [
                'name'                  => 'user',
                'display_name'          => 'User',
                'guard_name'            => 'web'
            ]
        );
        $user->givePermissionTo([
            'users_show',
        ]);
    }
}
