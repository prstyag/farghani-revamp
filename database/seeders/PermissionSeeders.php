<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        DB::table('permissions')->insert([
            [
                'name'              => 'users_access',
                'display_name'      => 'Users Access',
                'guard_name'        => 'web',
            ],
            [
                'name'              => 'users_create',
                'display_name'      => 'Users Create',
                'guard_name'        => 'web',
            ],
            [
                'name'              => 'users_edit',
                'display_name'      => 'Users Edit',
                'guard_name'        => 'web',
            ],
            [
                'name'              => 'users_show',
                'display_name'      => 'Users Show',
                'guard_name'        => 'web',
            ],
            [
                'name'              => 'users_delete',
                'display_name'      => 'Users Delete',
                'guard_name'        => 'web',
            ]
        ]);
    }
}
