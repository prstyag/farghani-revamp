<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Created User with role Super Admin
        $admin = User::create([
            'username'                      => "SkyyaTzy",
            'name'                          => "s_admin",
            'email'                         => "s_admin@gmail.com",
            'password'                      => Hash::make('password'),
            'created_at'                    => date('Y-m-d H:i:s'),
            'updated_at'                    => date('Y-m-d H:i:s'),
        ]);

        $admin->assignRole('super_admin');


        // Created User with role Admin
        $user = User::create(
            [
                'username'                      => "Admin",
                'name'                          => "admin",
                'email'                         => "admin@gmail.com",
                'password'                      => Hash::make('password'),
                'created_at'                    => date('Y-m-d H:i:s'),
                'updated_at'                    => date('Y-m-d H:i:s'),
            ]
        );
        
        $user->assignRole('admin');


        // Created User with role Writer
        $user = User::create(
            [
                'username'                      => "Writer",
                'name'                          => "writer",
                'email'                         => "writer@gmail.com",
                'password'                      => Hash::make('password'),
                'created_at'                    => date('Y-m-d H:i:s'),
                'updated_at'                    => date('Y-m-d H:i:s'),
            ]
        );
        
        $user->assignRole('writer');

        // Created User with role User
        $user = User::create(
            [
                'username'                      => "User",
                'name'                          => "user",
                'email'                         => "user@gmail.com",
                'password'                      => Hash::make('password'),
                'created_at'                    => date('Y-m-d H:i:s'),
                'updated_at'                    => date('Y-m-d H:i:s'),
            ]
        );
        
        $user->assignRole('user');

    }
}
